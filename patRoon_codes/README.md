## Project dependencies

The "renv.lock" file lists all the packages and versions that were installed and used here.

To sep up the same environment, please install the "renv" package in R and run the following command:


    renv::restore()
    
## Non-target screening with the PubChemLite for exposomics (PCL) database
"Non_target_POS.r" and "Non_target_NEG.r" contain the parameters employed for non-target screening in positive and negative ionization modes, respectively. Note that same parameters were employed for the serum and dust analysis.

Latest version of PCL can be found in [Zenodo](https://zenodo.org/records/10695523).

## Suspect Screening with the LRRK2 and INDOORCT16 lists
"SUSPECT_SCREENING_pos.r" and "SUSPECT_SCREENING_neg.r" show the parameters employed for the suspect screening in positive and negative ionization modes,respectively. Note that three different lists were tested, as shown in the suspects directory.