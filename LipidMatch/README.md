# Lipidomics Analysis using LipidMatch
"LipidMatch_POS.r" and "LipidMatch_NEG.r" contain the parameters employed for the lipidomics analysis in positive and negative ionization modes, respectively.

R version 3.3.3. was used to run the experiments, as suggested in the LipidMatch manual. Check [paper](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-017-1744-3) and[GitHub](https://github.com/GarrettLab-UF/LipidMatch) for details.

Note that MS-DIAL was employed for peak picking and alignment. Details about this analysis can be found in the manuscript and associated supporting information.


