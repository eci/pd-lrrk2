# Parkinson's disease associated LRRK2 study in dust and serum



## patRoon non-target and suspect screeening of dust and serum (polar extracts)
The patRoon_files folder contains all necessary databases and suspect lists.
The patRoon_codes folder contains the codes used to run patRoon.

## Metagenomics and omics integration of dust samples
The folder "Metagenomics" contains the codes related with the metagenomics analysis as well as its integration with the metabolomics/exposomics datasets (DIABLO analysis).

## LipidMatch analysis of serum (non-polar extracts)
The LipidMatch folder contains the codes employed for the data analysis with LipidMatch.
LipidMatch databases and code were downloaded from the [Innovative Omics website](https://innovativeomics.com/software/lipidmatch-suite/).

