## Suspect lists and databases employed for suspect and non-target screening in patRoon

## Suspect Lists
"INDOORCT16_LCMS_suspect_list" is a list containing 969 compounds created based on the chemicals identified in dust by LC-MS in the NORMAN collaborative trial.
"INDOORCT16_DSFP_suspect_list" is a list containing 497 compounds derived from the NORMAN Digital Sample Freezing Platform (DSFP) output.

The collection associated with list  INDOORCT16 on the NORMAN Suspect List Exchange can be freely downloaded on [Zenodo](https://zenodo.org/records/6848859)
Further details can be found in the [publication](https://link.springer.com/article/10.1007/s00216-019-01615-6#Sec20)

"LRRK2_suspect_list_01122022" is a list of chemicals specifically related with the LRRK2 gene. This was created using PubChem neighbor functionality which explores the relationship between chemicals and genes. This approach was previously applied in our [PD](https://link.springer.com/article/10.1007/s00216-022-04207-z) and [AD](https://pubs.acs.org/doi/full/10.1021/acs.est.3c10490) works. 

## Database
PubChem Lite for Exposomics (PCL) database (version 1.16.0, 26th November 2022) was employed as database for both non-target and suspect screening approaches.
This database contained 451,579 chemicals and can be downloaded [here](https://zenodo.org/records/7364484)
However, note that this database is dynamic and monthly updated, latest version can be found [here](https://zenodo.org/records/10695523)